import requests
import json 

apikey = 'e6d36662473f42a3af28e65ed6e3a4ba'
place = input("Enter the place name:-") 

url = 'https://api.opencagedata.com/geocode/v1/json?q='+place+'&key='+apikey+'&language=en&pretty=1'

locationOfPlace = requests.get(url)
locationOfPlace = locationOfPlace.json()['results']

try:
  geoCoordinates = locationOfPlace[0]['geometry']
  print("Longitude:",geoCoordinates['lng'],"\n","Latitude:",geoCoordinates['lat'])

except:
  print("Invalid place name or place does not exists")