#include <iostream>
#include <cstring>
#include <ctime>
#include <stdlib.h>
using namespace std;

class FoodContainer
{   
	float foodCount=10000;
	
    public:
	float getFoodCount() 
	{       
		return foodCount;    
	}
 
	void setFoodCount(float newFoodCount) 
	{   
    	foodCount = newFoodCount;  
	}
	
	void addFoodCount(float depositFood)
	{      
  		foodCount += depositFood;  
	}
	
	void subtractFoodCount(float debitFood) 
	{      
  		foodCount =foodCount- debitFood;   
 	}

};

class Animal
{
	public:	
	FoodContainer petFood;
	bool hungerStatus;

	bool getHungerStatus()
	{   
		int randomNumber = rand();
		if (randomNumber % 2 == 0)
		{
			hungerStatus = "true";
		}
		else
		{
			hungerStatus = "false";
		}

		return hungerStatus;
	}
	float getFood(float hungerLevel)
	{
		if (petFood.getFoodCount() > hungerLevel)
		{
			petFood.subtractFoodCount(hungerLevel);
			return hungerLevel;
		}
		else
		{
			petFood.subtractFoodCount(hungerLevel);
			return 1;
		}
	}
	float getHungerLevel()
	{
		float hungerLevel = rand()%100;
		return hungerLevel;
	}
};

class Owner
{ 	
	Animal myPet;

	public:
	bool hungerStatus;    
 	int feedAnimal(bool hungerStatus)
 	{
 		if(hungerStatus)
    		{   
    		    float hungerLevel = myPet.getHungerLevel();
				float foodConsumed = myPet.getFood(hungerLevel); 
		
				if (foodConsumed == hungerLevel) 
					{       
						cout<<"Animal has Fed."<<endl; 
					} 
				else
					{      
						cout<<"Not Enough Food in The Food Container, Whatever food is there has eaten by animal."<<endl; 
 					}
 	  		}
		else
			{
				cout<<"Animal is not hungry right now."<<endl;	
			} 
			  
	}
};

 
int main()
{   
	bool hungerStatus = "false";
	Animal myPet;
	Owner Devraj;
    hungerStatus=myPet.getHungerStatus();
    Devraj.feedAnimal(hungerStatus);
 	return 0;
}


