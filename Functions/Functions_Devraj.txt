Assignment 1: The below program is to guess the correct number between 1 to 100

import random
def generate_random_number(start_and_end_range):
    number=random.randint(int(start_and_end_range[0]),int(start_and_end_range[1]))
    return number

def check_range_for_user_choice(user_choice,start_and_end_range):
    if user_choice.isdigit() and int(start_and_end_range[0])<= int(user_choice) <=int(start_and_end_range[1]):
       return True
    else:
       return False

def get_range_from_user():
    range=input("Enter the range infornat start-end(ex:1-10):")
    start_and_end_range=list(range.split('-'))
    return start_and_end_range

def compare_user_choice_with_generated_number(user_choice,number):
    user_choice=int(user_choice)
    if user_choice<number:
        print("Too low. Guess again")
        return False
    elif user_choice>number:
        print("Too High. Guess again")
        return False
    else:
        return True
        
def get_number_of_guesses(user_choice,number,start_and_end_range):
    flag=False
    number_of_guess=0 
    while not flag:
        if not check_range_for_user_choice(user_choice,start_and_end_range):
            user_choice=input("I wont count this one Please enter a number between "+start_and_end_range[0]+" to "+ start_and_end_range[1]+":")
            continue
        else:
            number_of_guess+=1
            flag=compare_user_choice_with_generated_number(user_choice,number)
            if(flag==True):
                print("You guessed it in",number_of_guess,"guesses!")
                break
            user_choice=input()
                
def main():
    start_and_end_range=get_range_from_user()
    number=generate_random_number(start_and_end_range)
    user_choice=input("Guess a number between "+start_and_end_range[0]+" and "+start_and_end_range[1]+" :")
    get_number_of_guesses(user_choice,number,start_and_end_range)
    
        
main()










