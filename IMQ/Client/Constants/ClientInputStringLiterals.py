IMQ_HELP_MESSAGE = '-----Welcome To The IMQ Help Section-----\n'
START_COMMAND ='Command To Start The Application:- imq connect\n'
TOPIC_LIST_COMMAND = 'Command To Show Topic List:- imq show-topic-status\n'
MESSAGE_SUBSCRIBE_COMMAND = 'Command To Subscribe:- imq subscribe TOPIC_NAME\n'
MESSAGE_PUBLISH_COMMAND = 'Command for Publisher:- imq publish TOPIC_NAME -m MESSAGE_CONTENT\n'
CONNECTION_CLOSE_COMMAND = 'Command To Close The Connection:- imq disconnect\n'
EXTRA_INSTRUCTIONS = '**Client ID should be Integer within range 1 to 9999 and first digit should not be 0** \n **You can use either upper case or lower case for all the string commands**\n'

IMQ_WELCOME_MESSAGE = "Welcome to the Queue\n"
IMQ_START_COMMAND = 'imq connect'
SUBSCRIBER_BASE_COMMAND = 'imq subscribe'
PUBLISHER_BASE_COMMAND = 'imq publish'
TOPIC_STATUS_COMMAND =  'imq show-topic-status'
IMQ_DISCONNECT_COMMAND = 'imq disconnect'
IMQ_HELP_COMMAND = 'help'
IMQ_SUBSCRIBE = 'subscribe'
IMQ_PUBLISH =  'publish'
MESSAGE_PART_SEPARATOR = '-m'

IMQ_START_INPUT = "Please Enter Command to Connect with Queue\n Type Help To See Suggestions\n"
WRONG_INPUT_MESSAGE = 'You have entered wrong command\n Type Help To See Suggestions\n'
TOPIC_NOT_AVAILABLE_MESSAGE = 'You have entered a topic which is not available on server\nType Help To See Suggestions\n'
IMQ_DISCONNECT_INPUT = "Enter command to disconnect or press any alphabet to Continue\nType Help To See Suggestions\n"
CLIENT_ID_INPUT = "Please Enter your unique ID:\n"
WRONG_CLIENT_ID_MESSAGE = "Wrong input provided \n You Must Enter A Integer Digit Within Range 1 to 9999 and First digit should not be 0\n"
INPUT_TOPIC_LIST_MESSAGE = "Enter command to get Topic List\n"
COMMUNICATION_COMMAND_INPUT = "Please Enter Command to Publish Or Read Messages\n"



