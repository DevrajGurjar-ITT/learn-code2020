from Constants.Constants import *
from dataclasses import dataclass


@dataclass
class IMQProtocol:
    data: str
    sourceURL: str
    destinationURL: str
    topicName: str = DEFAULT_TOPIC_NAME
    dataFormat: str = DATA_FORMAT
    version: str = VERSION
    
