import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
from Constants.ExceptionStringLiterals import *

class ClientException(Exception):

    def printExceptionMessage(self):
        pass

class SocketErrorException(ClientException):
    
    def printExceptionMessage(self):
        print(SOCKET_CREATE_ERROR_MESSAGE)
        exit()

class ConnectionFailedException(ClientException):

    def printExceptionMessage(self):
        print(CONNECT_TO_SERVER_ERROR_MESSAGE)
        exit()

class SendMessageErrorException(ClientException):

    def printExceptionMessage(self):
        print(SEND_MESSAGE_ERROR)
        exit()

class RceiverMessageErrorException(ClientException):
    
    def printExceptionMessage(self):
        print(RECEIVE_MESSAGE_ERROR)
        exit()

class TypeConverionErrorException(ClientException):
    
    def printExceptionMessage(self):
        print(TYPE_CONVERSION_ERROR_MESSAGE)
        exit()

class DataFormateErrorException(ClientException):
    
    def printExceptionMessage(self):
        print(DATA_FORMATE_ERROR_MESSAGE)
        exit()

class ParsingErrorException(ClientException):
    
    def printExceptionMessage(self):
        print(PARSING_ERROR_MESSAGE)
        exit()

class WrongInputException(ClientException):
    message : None
    def printExceptionMessage(self, message):
        self.message = message
        print(message)
        exit()

class CloseSocketException(ClientException):
    
    def printExceptionMessage(self):
        print(CONNECTION_CLOSE_ERROR)
        exit()
