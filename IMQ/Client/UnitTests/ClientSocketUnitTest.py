import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
import unittest
import socket
from contextlib import contextmanager
from unittest.mock import Mock, create_autospec, patch
from unittest.mock import patch
from Constants.Constants import *
from Client.ClientSocket import *
from Constants.UnitTestConstants import *

class ClientSocketUnitTest(unittest.TestCase):

    __clientSocketObject = ClientSocket()
    __clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def testIsInstance(self):
        self.assertIsInstance(self.__clientSocketObject, ClientSocket)

    @patch('builtins.print')
    def testSocketCreation(self, mock_print):
        socketReceived = self.__clientSocketObject.createSocket()
        self.assertTrue(socketReceived)
        socketReceived.close()

    @patch('builtins.print')
    def testSocketType(self, mock_print):
        socketReceived = self.__clientSocketObject.createSocket()
        self.assertEqual(type(socketReceived), type(self.__clientSocket))
        socketReceived.close()

    def testSocketBinding(self):
        self.assertIsNone(self.__clientSocketObject.connectToServer(self.__clientSocket))

    def testsendMessageToServer(self):
        self.assertIsNone(self.__clientSocketObject.sendMessageToServer(self.__clientSocket, ID))

    def testrecieveFromServerCalled(self):
            message = str.encode(ID)
            mockSocket = create_autospec(socket.socket)
            mockSocket.recv.return_value = message
            self.__clientSocketObject.receiveMessageFromServer(mockSocket)
            assert mockSocket.recv.called
        

if __name__ == '__main__':
    unittest.main()