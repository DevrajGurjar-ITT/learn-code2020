import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
import unittest
from contextlib import contextmanager
from unittest.mock import patch
from Constants.Constants import *
from Client.ClientUtility import *
from Constants.UnitTestConstants import *

class ClientUtilityUnitTest(unittest.TestCase):

    __clientUtilityObject = ClientUtility()

    def testIsInstance(self):
        self.assertIsInstance(self.__clientUtilityObject, ClientUtility)

    def testconvertDictToList(self):
        self.assertTrue(self.__clientUtilityObject.convertDictToList(MOCK_DICT))

    def testconvertStrToDictionary(self):
        self.assertTrue(self.__clientUtilityObject.convertStrToDictionary(MOCK_LIST_STR))

    @patch('builtins.print')
    def testshowTopicListToClient(self, mock_print):
        self.assertIsNone(self.__clientUtilityObject.showTopicListToClient(MOCK_LIST))

if __name__ == '__main__':
    unittest.main()
