import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)

from UnitTests.ClientInputUnitTest import *
from UnitTests.ClientServiceUnitTest import *
from UnitTests.ClientSocketUnitTest import *
from UnitTests.ClientUtilityUnitTest import *

if __name__ == '__main__':
    unittest.main()