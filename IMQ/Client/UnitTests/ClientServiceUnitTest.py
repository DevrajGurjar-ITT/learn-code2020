import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
import unittest
import socket
from contextlib import contextmanager
from unittest.mock import patch
from Constants.ExceptionStringLiterals import *
from Constants.UnitTestConstants import *
from Constants.Constants import *
from Client.ClientService import *
from Client.ClientSocket import *
import ParserPKG as Parser

class ClientServiceUnitTest(unittest.TestCase):

    __parserObject = Parser.Parser()
    exceptionObject = ClientException()

    def testIsInstanceClientException(self):
        self.assertIsInstance(self.exceptionObject, Exception)

    def testIsInstanceOfParser(self):
        self.assertIsInstance(self.__parserObject, Parser.Parser)

    def testSerializeToJsonConvertor(self):
        response = self.__parserObject.serializeToJsonConvertor(MOCK_MESSAGE)
        self.assertEqual(response, RESPONSE_MOCK_MESSAGE)

    def testDeserializeJsonFormattedData(self):
        serializeResponse = self.__parserObject.serializeToJsonConvertor(MOCK_MESSAGE)
        deserializeResponse = self.__parserObject.deserializeJsonFormattedData(serializeResponse)
        self.assertEqual(deserializeResponse, MOCK_MESSAGE)

    
if __name__ == '__main__':
    unittest.main()