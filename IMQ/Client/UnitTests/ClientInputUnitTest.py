import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
import unittest
import socket
from contextlib import contextmanager
from unittest.mock import patch
from Constants.ExceptionStringLiterals import *
from Constants.Constants import *
from Client.ClientInputHandler import *
from Constants.UnitTestConstants import *


class ClientInputUnitTest(unittest.TestCase):

    __clientInputObject = ClientInputHandler()

    def testIsInstance(self):
        self.assertIsInstance(self.__clientInputObject, ClientInputHandler)
    
    def testSubscriberAttributes(self):
        topicReceived = self.__clientInputObject.getSubscriberAttributes('imq subscribe Technical')
        self.assertEqual(topicReceived, 'TECHNICAL')

    @patch('builtins.print')
    def testShowCommandsToUser(self, mock_print):
        self.assertIsNone(self.__clientInputObject.showCommandsToUser())

    @patch('builtins.input', return_value = message)
    def testStartApplicationInupt(self, mock_input):
        self.assertIsNone(self.__clientInputObject.takeInputToStartApplication())
        
    @patch('builtins.input', return_value = TOPIC_STATUS_COMMAND)
    def testInputForTopicStatus(self, mock_input):
        self.assertIsNone(self.__clientInputObject.takeInputForTopicStatus())

    @patch('builtins.input', return_value = message)
    def testInputToCommunicateWithQueue(self, mock_input):
        result = self.__clientInputObject.takeInputToCommunicateWithQueue()
        self.assertEqual(result , message)

    @patch('builtins.print')
    def testVarifySizeOfClientInput(self, mock_print):
        self.assertTrue(self.__clientInputObject.varifySizeOfClientInput(ID))

    @patch('builtins.print')
    def testVarifyClientInputSize(self, mock_print):
        self.assertIsNone(self.__clientInputObject.varifySizeOfClientInput(MESSAGE))

    @patch('builtins.print')
    def testDecideClientRole(self, mock_print):
        self.assertIsNone(self.__clientInputObject.decideClientRole(message))

    @patch('builtins.print')
    def testSubscriberRole(self, mock_print):
        self.assertEqual(self.__clientInputObject.decideClientRole(SUBSCRIBER_COMMAND), SUBSCRIBER)

    @patch('builtins.print')
    def testPublisherRole(self, mock_print):
        self.assertEqual(self.__clientInputObject.decideClientRole(PUBLISHER_COMMAND), PUBLISHER)
    
    @patch('builtins.print')
    def testSubscriberCommand(self, mock_print):
        self.assertFalse(self.__clientInputObject.varifySubscriberCommand(SUBSCRIBER_COMMAND))

    @patch('builtins.print')
    def testPublisherCommand(self, mock_print):
        self.assertFalse(self.__clientInputObject.varifyPublisherInput(PUBLISHER_COMMAND))

    @patch('builtins.print')
    def testClientDisconnectCommand(self, mock_print):
        self.assertTrue(self.__clientInputObject.checkIfClientWantsTODisconnect(DISCONNECT_COMMAND))

    @patch('builtins.print')
    def testGetPublisherAttributes(self, mock_print):
        self.assertTrue(self.__clientInputObject.getPublisherAttributes(PUBLISHER_COMMAND))

    @patch('builtins.print')
    def testVarifySubscriberCommand(self, mock_print):
        self.assertTrue(self.__clientInputObject.varifySubscriberCommand(PUBLISHER_COMMAND))

    @patch('builtins.print')
    def testVarifyTopicForSubscriber(self, mock_print):
        self.assertTrue(self.__clientInputObject.varifyTopicForSubscriber(MOCK_TOPIC_LIST, SUBSCRIBER_COMMAND))
    

if __name__ == '__main__':
    unittest.main()
