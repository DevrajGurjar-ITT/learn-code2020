import socket
import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
import ast
from Constants.Constants import *
from IMQProtocol.Request import *
from IMQProtocol.Parser import *
from Constants.ExceptionStringLiterals import *
from ClientExceptions.ClientException import *
from Client.ClientSocket import *
from Client.ClientInputHandler import *
from Client.ClientUtility import *
import ParserPKG

class ClientService:

    parserObject = ParserPKG.Parser()
    InputHandlerObject : None 
    clientSocketObject : None 

    def __init__(self, InputHandlerObject, clientSocketObject):
        self.InputHandlerObject = InputHandlerObject
        self.clientSocketObject = clientSocketObject

    def sendRequestObjectToServer(self, clientSocket, request):
        requestObject = Request(request, HOST, HOST)
        self.__sendSerializedRequestObject(requestObject, clientSocket)

    def __sendSerializedRequestObject(self, requestObject, clientSocket):
        try:
            request = self.parserObject.serializeToJsonConvertor(requestObject)
        except:
            raise ParsingErrorException()
        try:
            self.clientSocketObject.sendMessageToServer(clientSocket, request)
        except SendMessageErrorException as error:
            error.printExceptionMessage()

    def receiveResponseFromServer(self, clientSocket):
        try:
            response = self.clientSocketObject.receiveMessageFromServer(clientSocket)
        except RceiverMessageErrorException as error:
            error.printExceptionMessage()
        try:
            responseJsonMessage = self.parserObject.deserializeJsonFormattedData(response)
        except:
            raise ParsingErrorException()
        return responseJsonMessage

    def communicationWithServer(self, clientSocket, clientRole, clientInput):
        if(clientRole==PUBLISHER):
            publisherAttributes = self.InputHandlerObject.getPublisherAttributes(clientInput)
            self.sendRequestObjectToServer(clientSocket, PUBLISHER)
            self.__publishMessageOnServer(clientSocket, publisherAttributes)
        else:
            clientTopic = self.InputHandlerObject.getSubscriberAttributes(clientInput)
            self.sendRequestObjectToServer(clientSocket, SUBSCRIBER)
            self.sendRequestObjectToServer(clientSocket, clientTopic)
            try:
                self.__receiveSubscribedMessageFromQueue(clientSocket)
            except DataFormateErrorException as error:
                error.printExceptionMessage()

    def __publishMessageOnServer(self, clientSocket, publisherAttributes):
        message = publisherAttributes[0]
        topicName = publisherAttributes[1]
        requestObject = Request(message, HOST, HOST, topicName)
        try:
            self.__sendSerializedRequestObject(requestObject, clientSocket)
            responseJsonMessage = self.receiveResponseFromServer(clientSocket)
        except ParsingErrorException as error:
            error.printExceptionMessage()
        print(SERVER_RESPONSE,responseJsonMessage[MESSAGE_DATA] +
                 NEW_LINE + responseJsonMessage[MESSAGE_STATUS])

    def __receiveSubscribedMessageFromQueue(self, clientSocket):
            responseJsonMessage = self.receiveResponseFromServer(clientSocket)
            for index in range(0, len(responseJsonMessage[MESSAGE_DATA])):
                print(FORMAT_TYPE_ONE.format(index+1), FORMAT_TYPE_TWO.format(responseJsonMessage[MESSAGE_DATA][index]))


 

