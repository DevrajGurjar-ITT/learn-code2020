import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
from Constants.Constants import *
from ClientExceptions.ClientException import *
from Constants.ExceptionStringLiterals import *

class ClientUtility:

    def modifyTopicList(self, topicString):
        topicDict = self.convertStrToDictionary(topicString)
        topicList = self.convertDictToList(topicDict)
        return topicList

    def convertStrToDictionary(self, topicString):
        try:
            topicDict = eval(topicString)
            return topicDict
        except:
            raise TypeConverionErrorException()

    def convertDictToList(self, topicDict):
        try:
            topicList = list(topicDict.values())
            return topicList
        except:
            raise TypeConverionErrorException()

    def showTopicListToClient(self, topicList):
        try:
            print(TOPIC_AVAILABLE)
            if(len(topicList) == 0):
                print(TOPIC_NOT_AVAILABLE)
            for index in range(0, len(topicList)):
                print(FORMAT_TYPE_ONE.format(index+1), FORMAT_TYPE_TWO.format(topicList[index]))
        except:
            raise DataFormateErrorException()

