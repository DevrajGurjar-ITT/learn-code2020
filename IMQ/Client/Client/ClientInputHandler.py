from Constants.ClientInputStringLiterals import *
from Constants.Constants import *
from ClientExceptions.ClientException import *

class ClientInputHandler:
    
    def showCommandsToUser(self):
        print(IMQ_HELP_MESSAGE, START_COMMAND, TOPIC_LIST_COMMAND, MESSAGE_SUBSCRIBE_COMMAND, MESSAGE_PUBLISH_COMMAND, CONNECTION_CLOSE_COMMAND, EXTRA_INSTRUCTIONS)

    def takeInputToStartApplication(self):
        startApplicationCommand = input(IMQ_START_INPUT)
        while startApplicationCommand.lower() != IMQ_START_COMMAND:
            if startApplicationCommand.lower() == IMQ_START_COMMAND:
                print(IMQ_WELCOME_MESSAGE)
            elif startApplicationCommand.lower() == IMQ_HELP_COMMAND:
                self.showCommandsToUser()
            else:
                print(WRONG_INPUT_MESSAGE)
            startApplicationCommand = input(IMQ_START_INPUT)

    def getClientID(self):
        clientId = EMPTY_SPACE
        while clientId.isdigit() == False or int(clientId) > CLIENT_INPUT_UPPER_RANGE or int(clientId)< CLIENT_INPUT_LOWER_RANGE or clientId[0] == ZERO_INDEX:
            clientId = input(CLIENT_ID_INPUT)
            if clientId.isdigit() == False or int(clientId) > CLIENT_INPUT_UPPER_RANGE or int(clientId)< CLIENT_INPUT_LOWER_RANGE or clientId[0] == ZERO_INDEX:
                print(WRONG_CLIENT_ID_MESSAGE)
            else:
                return int(clientId)

    def takeInputForTopicStatus(self):
        userInput = input(INPUT_TOPIC_LIST_MESSAGE)
        while userInput.lower() != TOPIC_STATUS_COMMAND:
            if userInput.lower() == TOPIC_STATUS_COMMAND:
                return userInput
            elif userInput.lower() == IMQ_HELP_COMMAND:
                self.showCommandsToUser()
            else:
                print(WRONG_INPUT_MESSAGE)
            userInput = input(INPUT_TOPIC_LIST_MESSAGE)

    def takeInputToCommunicateWithQueue(self):
        clientInput = input(COMMUNICATION_COMMAND_INPUT)
        return clientInput

    def processClientInput(self, topicList):
        while True:
            clientInput = self.takeInputToCommunicateWithQueue()
            
            if(self.varifySizeOfClientInput(clientInput)):
                continue
            if(clientInput.lower() == IMQ_HELP_COMMAND):
                self.showCommandsToUser()
                continue
            clientRole = self.decideClientRole(clientInput)
            if(clientRole is None):
                continue
            if (self.varifyClientInput(topicList, clientInput, clientRole)):
                continue
            else:
                break
        return clientInput, clientRole

    def varifySizeOfClientInput(self, clientInput):
        if(len(clientInput) < MIN_CLIENT_INPUT_SIZE):
            print(WRONG_INPUT_MESSAGE)
            return True

    def decideClientRole(self, clientInput):
        if(len(clientInput.split(EMPTY_SPACE)) < CLIENT_ROLE_THRESHOLD):
            print(WRONG_INPUT_MESSAGE)
            return None
        if(clientInput.split(EMPTY_SPACE)[1].lower() == IMQ_SUBSCRIBE and len(clientInput.split(EMPTY_SPACE)) == SUBSCRIBER_INPUT_SIZE):
            return SUBSCRIBER
        elif(clientInput.split(EMPTY_SPACE)[1].lower() == IMQ_PUBLISH and len(clientInput.split(EMPTY_SPACE)) >= PUBLISHER_INPUT_SIZE):
            return PUBLISHER
        else:
            print(WRONG_INPUT_MESSAGE)
            return None
        
    def varifyClientInput(self, topicList, clientInput, clientRole):
        if(clientRole == SUBSCRIBER): 
            topicStatus = self.varifyTopicForSubscriber(topicList, clientInput)
            commandStatus = self.varifySubscriberCommand(clientInput)
            if(topicStatus == False and commandStatus == False):
                result = False
            else:
                result = True
        else:
            result = self.varifyPublisherInput(clientInput)
        return result

    def varifySubscriberCommand(self, clientInput):
        clientInput = clientInput.split(EMPTY_SPACE)
        if((clientInput[0]+EMPTY_SPACE+clientInput[1]).lower() != SUBSCRIBER_BASE_COMMAND):
            print(WRONG_INPUT_MESSAGE)
            return True
        else:
            return False

    def varifyPublisherInput(self, clientInput):
        clientInput = clientInput.split(EMPTY_SPACE)
        if((clientInput[0]+ EMPTY_SPACE +clientInput[1]).lower() != PUBLISHER_BASE_COMMAND):
            flag = True
            print(WRONG_INPUT_MESSAGE)
        elif(clientInput[3].lower() != MESSAGE_PART_SEPARATOR):
            flag = True
            print(WRONG_INPUT_MESSAGE)
        else:
            flag = False
        return flag

    def varifyTopicForSubscriber(self, topicList, clientInput):
        subscriberTopic = self.getSubscriberAttributes(clientInput)
        if( subscriberTopic in topicList):
            return False
        else:
            print(TOPIC_NOT_AVAILABLE_MESSAGE)
            return True

    def getPublisherAttributes(self, clientInput):
        clientMessage = clientInput.split(MESSAGE_PART_SEPARATOR)[1][1:]
        topicName = clientInput.split(EMPTY_SPACE)[2].upper()
        return clientMessage, topicName

    def getSubscriberAttributes(self, clientInput):
        subscriberTopic = clientInput.split(EMPTY_SPACE)[2].upper()
        return subscriberTopic

    def takeInputToDisconnectClient(self):
        clientInput = input(IMQ_DISCONNECT_INPUT)
        if clientInput.lower() == IMQ_HELP_COMMAND:
            while True:
                self.showCommandsToUser()
                clientInput = input(IMQ_DISCONNECT_INPUT)
                if clientInput.lower() != IMQ_HELP_COMMAND:
                    break
        return clientInput

    def checkIfClientWantsTODisconnect(self, clientInput):
        if clientInput.lower() == IMQ_DISCONNECT_COMMAND:
            return True