import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
import ast
from Constants.Constants import *
from Constants.ExceptionStringLiterals import *
from ClientExceptions.ClientException import *
from Client.ClientSocket import *
from Client.ClientInputHandler import *
from Client.ClientUtility import *
from Client.ClientService import *


def clientStart():
    
    try:
        clientInputObject = ClientInputHandler()
        clientSocketObject = ClientSocket()
        clientUtilityObject = ClientUtility()
        clientObject = ClientService(clientInputObject, clientSocketObject)

        try:
            clientSocket = clientSocketObject.createSocket()
        except SocketErrorException as error:
            error.printExceptionMessage()

        clientInputObject.takeInputToStartApplication()
        try:
            clientSocketObject.connectToServer(clientSocket)
        except ConnectionFailedException as error:
            error.printExceptionMessage()

        welcomeResponseFromServer = clientObject.receiveResponseFromServer(clientSocket)
        print(welcomeResponseFromServer[MESSAGE_DATA])
        clientID = clientInputObject.getClientID()
        clientObject.sendRequestObjectToServer(clientSocket, clientID)

        while True:
            userInput = clientInputObject.takeInputForTopicStatus()
            try:
                topicList = clientObject.receiveResponseFromServer(clientSocket)
            except RceiverMessageErrorException as error:
                error.printExceptionMessage()
            topicList = topicList[MESSAGE_DATA]
            try:
                topicList = clientUtilityObject.modifyTopicList(topicList) #Function to change Topic List Format
            except TypeConverionErrorException as error:
                error.printExceptionMessage()

            clientUtilityObject.showTopicListToClient(topicList)
            clientInput , clientRole = clientInputObject.processClientInput(topicList) #Function to Varify Command.
            clientObject.communicationWithServer(clientSocket, clientRole, clientInput)
            clientInput = clientInputObject.takeInputToDisconnectClient()
            clientObject.sendRequestObjectToServer(clientSocket, clientInput)
            if(clientInputObject.checkIfClientWantsTODisconnect(clientInput)):
                break
        try:    
            clientSocketObject.closeSocket(clientSocket)
        except CloseSocketException as error:
            error.printExceptionMessage()

    except:
        print(CONNECTION_CLOSE_ERROR)


if __name__ == '__main__':
    clientStart()
