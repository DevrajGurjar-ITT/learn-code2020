import sys
import ast
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
import socket
from Constants.Constants import *
from Constants.ExceptionStringLiterals import *
from ClientExceptions.ClientException import *

class ClientSocket:

    def createSocket(self):
        try:
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            return clientSocket
        except:
            raise SocketErrorException()

    def connectToServer(self, clientSocket):
        try:
            clientSocket.connect((HOST,PORT))
        except:
            raise ConnectionFailedException()

    def receiveMessageFromServer(self, clientSocket):
        try:
            response = clientSocket.recv(MAX_MESSAGE_SIZE)
            return response.decode(DECODING_TYPE)
        except:
            raise RceiverMessageErrorException()

    def sendMessageToServer(self, clientSocket, message):
        try:
            clientSocket.send(str.encode(message))
        except:
            raise SendMessageErrorException()

    def closeSocket(self, clientSocket):
        try:
            clientSocket.close()
            return True
        except:
            raise CloseSocketException()
