import sys
import os
sys.path.append(os.getcwd())
from _thread import *
from Constants.Constants import *
from DataBase.Database import *
from Queue.Queue import *
from Server.ServerSocket import *
from Constants.ExceptionStringLiterals import *
from ServerException.ServerException import *
from Server.Container import *
from Server.ServerUtility import *
import ParserPKG as parserPackage
import datetime
from datetime import timedelta


class ServerService:

    serverExceptionObject = ServerException()
    parserObject = parserPackage.Parser()
    serverUtilityObject : None 
    serverSocketObject: None

    def __init__(self, serverSocketObject, serverUtilityObject):
        self.serverSocketObject = serverSocketObject
        self.serverUtilityObject = serverUtilityObject

    def receiveCommandFromClient(self, clientConnection):
        response = self.serverSocketObject.receiveMessageFromClient(clientConnection)
        clientJsonMessage = self.parserObject.deserializeJsonFormattedData(response)
        return clientJsonMessage

    def sendAcknowledgementToClient(self, responseMessage, clientConnection):
        responseObject = self.serverUtilityObject.getResponseObject(responseMessage)
        response = self.parserObject.serializeToJsonConvertor(responseObject)
        self.serverSocketObject.sendMessageToClient(clientConnection, response)

    def communicateWithPublisher(self, clientConnection, listOfQueue, databaseObject, clientID):
        try:
            currentDateTime = datetime.datetime.now()
            clientJsonMessage = self.receiveCommandFromClient(clientConnection)
            queueObject = listOfQueue[0]
            isNewTopic = databaseObject.checkExistanceOfTopic(clientJsonMessage[TOPIC_NAME])
            if(isNewTopic == 0):
                databaseObject.addTopicInDatabase(clientJsonMessage[TOPIC_NAME])
                listOfQueue.append(queueObject.initializeQueue())
            publisherRoleId = PUBLISHER_ROLE_ID
            message = clientJsonMessage[MESSAGE_DATA] + TILD_SEPARATOR + str(currentDateTime)
            print(MESSAGE_RECEIVED, message)
            topicId = databaseObject.getTopicIdFromDatabase(clientJsonMessage[TOPIC_NAME])
            queueId = listOfQueue[topicId]
            queueObject.addMessageToQueue(queueId, message)
            self.sendAcknowledgementToClient(clientJsonMessage[MESSAGE_DATA], clientConnection)
            try:
                databaseObject.saveServerClientDataToDataBase(int(clientID), publisherRoleId , topicId)
                databaseObject.savePublisherDataToDatabase(clientJsonMessage, clientID, topicId)
            except:
                raise DatabaseErrorException()
        except:
            self.sendAcknowledgementToClient(PUBLICATION_ERROR_MESSAGE, clientConnection)
            raise PublicationErrorException()

    def communicateWithSubscriber(self, clientConnection, listOfQueue, databaseObject, clientID):
        JsonData = self.receiveCommandFromClient(clientConnection)
        topicName = JsonData[MESSAGE_DATA]
        topicId = databaseObject.getTopicIdFromDatabase(topicName)
        isNewSubscriber = databaseObject.checkExistanceOfSubscriber(int(clientID), SUBSCRIBER_ROLE_ID, int(topicId))
        SubscriberRoleId = SUBSCRIBER_ROLE_ID
        clientAttributeObject = ClientContainer(SUBSCRIBER_ROLE_ID)
        clientAttributeObject.clientId = clientID
        clientAttributeObject.topicId = topicId
        clientAttributeObject.clientConnection = clientConnection
        if(isNewSubscriber == 0):
            self.handleNewSubscriber(databaseObject, clientAttributeObject, listOfQueue)
        else:
            self.handleOldSubscriber(databaseObject, clientAttributeObject, listOfQueue)

    def handleNewSubscriber(self, databaseObject, clientAttributeObject, listOfQueue):
        today = datetime.datetime.today()
        MessageWatchTime = today - timedelta(days = 365)
        queueObject = listOfQueue[0]
        queueId = listOfQueue[clientAttributeObject.topicId]
        databaseObject.saveServerClientDataToDataBase(clientAttributeObject.clientId,clientAttributeObject.SubscriberRoleId ,clientAttributeObject.topicId)
        returnMessageWatchTime = self.sendSubscribedMessageToServer(clientAttributeObject.clientConnection, queueId, queueObject, MessageWatchTime)
        databaseObject.updateMessageWatchTime(clientAttributeObject, returnMessageWatchTime)    

    def handleOldSubscriber(self, databaseObject, clientAttributeObject, listOfQueue):
        queueObject = listOfQueue[0]
        MessageWatchTime = databaseObject.getLastMessageWatchTime(clientAttributeObject)
        queueId = listOfQueue[clientAttributeObject.topicId]
        returnMessageWatchTime = self.sendSubscribedMessageToServer(clientAttributeObject.clientConnection, queueId, queueObject, MessageWatchTime)
        databaseObject.updateMessageWatchTime(clientAttributeObject, returnMessageWatchTime)    

    def sendSubscribedMessageToServer(self, clientConnection, messageQueue, queueObject, MessageWatchTime): 
        try:
            queueMessagesList = list(messageQueue.queue)
            queueRange = len(list(messageQueue.queue))
            messageList = []
            for i in range(0 , queueRange):
                dateTimePartOfQueueMessage = queueMessagesList[i].split(TILD_SEPARATOR)
                dateTimePartOfQueueMessage = datetime.datetime.strptime(dateTimePartOfQueueMessage[1], TIME_FORMATE)
                if(dateTimePartOfQueueMessage >= MessageWatchTime):
                    messageList.append(queueMessagesList[i])
            if not len(messageList):
                messageList.append(NO_MESSAGE_AVAILABLE)   
            self.sendAcknowledgementToClient(messageList, clientConnection)
            MessageWatchTime = datetime.datetime.now()
            return MessageWatchTime
        except:
            self.sendAcknowledgementToClient(SUBSCRIPTION_ERROR_MESSAGE, clientConnection)
            raise SubscriptionErrorException()

    def sendTopicListToClient(self, clientConnection, databaseObject):
        listOfTopics=databaseObject.getTopicListFromDatabase()
        self.sendAcknowledgementToClient(listOfTopics, clientConnection)

