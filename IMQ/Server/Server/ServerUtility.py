import sys
import os
sys.path.append(os.getcwd())
from Constants.Constants import *
from Constants.ExceptionStringLiterals import *
import datetime
import schedule
import time
from Queue.Queue import *
import ParserPKG as parserPackage
from Constants.ExceptionStringLiterals import *
from ServerException.ServerException import *
from IMQProtocol.Response import *


class ServerUtility:

    def decideClientRole(self, clientRequest):
        if(clientRequest[MESSAGE_DATA] == PUBLISHER):
            Ispublisher = 1
            return True
        else:
            IsSubscriber = 1
            return False

    def getResponseObject(self, messageForSubscriber):
        data = messageForSubscriber
        sourceIP = HOST
        desitantionIP = HOST
        responseObject = Response(data, sourceIP, desitantionIP)
        return responseObject
