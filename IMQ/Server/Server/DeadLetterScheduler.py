import sys
import os
sys.path.append(os.getcwd())
from _thread import *
import time
from Constants.Constants import *


class DeadLetterScheduler():

    deadMessageQueueObject : None 
    deadMessageQueue : None
    databaseObject : None
    
    def __init__(self, deadMessageQueueObject, databaseObject):
        self.deadMessageQueueObject = deadMessageQueueObject
        self.databaseObject = databaseObject
        self.deadMessageQueue = self.deadMessageQueueObject.initializeQueue()
 

    def removeDeadMessageFromQueue(self, queueList):
        import datetime
        while True:
            queueObject = queueList[0]

            for i in range(0, len(queueList)-1):
                queue = queueList[i+1]
                queueMessageList = list(queue.queue)
                queueLength = len(list(queue.queue))
                
                for j in range(0 , queueLength):
                    dateTimePartOfQueueMessage = queueMessageList[j].split(TILD_SEPARATOR)
                    currentDateTime = datetime.datetime.now()
                    queueMessageTimeStamp = datetime.datetime.strptime(dateTimePartOfQueueMessage[1], TIME_FORMATE)
                    timeDifference = (currentDateTime - queueMessageTimeStamp).total_seconds()
                    if(timeDifference >= TIME_LIMIT_FOR_DEAD_MESSAGE):
                        self.deadMessageQueueObject.addMessageToDeadQueue(self.deadMessageQueue, queueMessageList[j])
                        self.databaseObject.updateStatusOfDeadMessage(TIME_LIMIT_FOR_DEAD_MESSAGE)
                        queueObject.getMessageFromQueue(queue)

            time.sleep(5)
