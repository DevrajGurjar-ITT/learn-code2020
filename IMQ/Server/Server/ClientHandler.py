import sys
import os
sys.path.append(os.getcwd())
from _thread import *
from Constants.Constants import *
from DataBase.Database import *
from datetime import datetime
from Constants.ExceptionStringLiterals import *
from ServerException.ServerException import *
from Server.ServerSocket import *
from Server.ServerService import *
from Server.ServerUtility import *


class ClientHandler:

    serverSocketObject : None 
    serverExceptionObject : None 
    serverUtilityObject  : None
    ServerServiceObject : None 

    def __init__(self, serverSocketObject):
        self.serverSocketObject = serverSocketObject
        self.serverExceptionObject = ServerException()
        self.serverUtilityObject = ServerUtility()
        self.ServerServiceObject = ServerService(self.serverSocketObject, self.serverUtilityObject)

    def connectToClient(self, serverSocket, listOfQueue):
        while True:
            clientConnectionAttributes = self.serverSocketObject.makeConnectionToClient(serverSocket)
            clientConnection  = clientConnectionAttributes[0]
            IPAddress = clientConnectionAttributes[1]
            print(CONNECTED_TO + IPAddress[0] + COLON + str(IPAddress[1]))
            try:
                start_new_thread(self.startNewClientThread, (clientConnectionAttributes, listOfQueue))
            except: 
                clientConnection.close()

    def startNewClientThread(self, clientConnectionAttributes, listOfQueue):
        try:
            clientConnection  = clientConnectionAttributes[0]
            IPAddress = clientConnectionAttributes[1]
            self.ServerServiceObject.sendAcknowledgementToClient(WELCOME_EMESSAGE, clientConnection) 
            clientJsonData = self.ServerServiceObject.receiveCommandFromClient(clientConnection)
            clientID = clientJsonData[MESSAGE_DATA]
            databaseObject = Database.getInstance()
            clientName = IPAddress[0] + COLON + str(IPAddress[1])
            databaseObject.saveClientIdInDatabase(clientID ,clientName)              
            while True:
                self.ServerServiceObject.sendTopicListToClient(clientConnection, databaseObject)
                clientRequest = self.ServerServiceObject.receiveCommandFromClient(clientConnection)
                IsPubOrIsSub = self.serverUtilityObject.decideClientRole(clientRequest)
                if(IsPubOrIsSub == True):
                    try:
                        self.ServerServiceObject.communicateWithPublisher(clientConnection, listOfQueue, databaseObject, clientID)
                    except ServerException as error:
                        error.printExceptionMessage()
                else:
                    try:
                        self.ServerServiceObject.communicateWithSubscriber(clientConnection, listOfQueue, databaseObject, clientID)
                    except SubscriptionErrorException as error:
                        error.printExceptionMessage()
                clientRequest = self.ServerServiceObject.receiveCommandFromClient(clientConnection)
                if(clientRequest[MESSAGE_DATA] == DISCONNECT_COMMAND):
                    break
            clientConnection.close()
        except:
            print(THREAD_CLOSE_ERROR_MESSAGE)
            clientConnection.close()
