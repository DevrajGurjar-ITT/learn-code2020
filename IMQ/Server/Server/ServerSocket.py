import socket
import sys
import os
sys.path.append(os.getcwd())
from _thread import *
from Constants.Constants import *
from Constants.ExceptionStringLiterals import *
from ServerException.ServerException import *


class ServerSocket:

    def createSocket(self):
        try:
            serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            return serverSocket
        except:
            raise SocketErrorException()        

    def checkSocketStatus(self, serverSocket):
        if serverSocket is None:
            print(SOCKET_STATUS_ERROR)
        print(SOCKET_STATUS_SUCCESS)

    def bindServerSocket(self, serverSocket):
        try:
            serverSocket.bind((HOST, PORT))
        except:
            raise ConnectionBindErrorException()
        
    def putServerToListeningMode(self, serverSocket):
        try:
            serverSocket.listen(MAX_NO_OF_CONNCTIONS)
            print(SERVER_LISTENING_MESSAGE)
        except:
            raise SocketListenErrorException()       

    def makeConnectionToClient(self, serverSocket):
        try:
            return serverSocket.accept()
        except:
            raise ConnectionFailedException()
        
    def sendMessageToClient(self, connection, message):
        try:
            connection.sendall(str.encode(message))
        except:
            raise SendMessageErrorException()

    def receiveMessageFromClient(self, connection):
        try:
            data = connection.recv(MAX_MESSAGE_SIZE)
            return data.decode(DECODING_TYPE)
        except:
            raise RceiverMessageErrorException()
        
    def closeSocket(self, serverSocket):
        try:
            serverSocket.close()
            return True
        except:
            raise CloseSocketException()