import sys
import os
sys.path.append(os.getcwd())
from _thread import *
from Constants.Constants import *
from Queue.Queue import *
from Server.ServerSocket import *
from Server.DeadLetterScheduler import *
from Constants.ExceptionStringLiterals import *
from ServerException.ServerException import *
from Server.ServerService import *
from Server.ClientHandler import *


def main():

    try:
        serverExceptionObject = ServerException()
        serverSocketObject = ServerSocket()
        
        clientHandlerObject = ClientHandler(serverSocketObject)

        databaseObject = Database.getInstance()
        databaseObject.createDatabaseStructure()

        queueObject = Queue()
        deadLetterSchedulerObj = DeadLetterScheduler(queueObject, databaseObject)

        try:
            serverSocket = serverSocketObject.createSocket()
        except SocketErrorException as error:
            error.printExceptionMessage()

        serverSocketObject.checkSocketStatus(serverSocket)

        try:
            serverSocketObject.bindServerSocket(serverSocket)
        except SocketListenErrorException as error:
            error.printExceptionMessage()
        
        try:
            serverSocketObject.putServerToListeningMode(serverSocket)
        except SocketErrorException as error:
            error.printExceptionMessage()
        
        listOfQueue = queueObject.queueManager(databaseObject)

        start_new_thread(deadLetterSchedulerObj.removeDeadMessageFromQueue, (listOfQueue,))

        try:
            clientHandlerObject.connectToClient(serverSocket, listOfQueue)
        except ConnectionFailedException as error:
            error.printExceptionMessage()
        
        try:
            serverSocketObject.closeSocket(serverSocket)
        except CloseSocketException as error:
            error.printExceptionMessage()
    except:
        print(SERVER_CLOSE_ERROR_MESSAGE)

if __name__ == '__main__':
    main()
