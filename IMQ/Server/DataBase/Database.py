import sys
import os
sys.path.append(os.getcwd())
import pyodbc
from datetime import datetime
from Constants.Constants import *
from DataBase.DatabaseConstants import *
from DataBase.DatabaseQueries import *
from DataBase.DatabaseConstants import *

class Database:

    __databaseObject = None

    databaseConnection :None

    @staticmethod
    def getInstance():
        if Database.__databaseObject == None:
            Database()
            Database.__databaseObject.databaseConnection = pyodbc.connect(**DATABASE_CONNECTION_CONFIG)
        return Database.__databaseObject

    def __init__(self):
        if Database.__databaseObject != None:
            raise Exception(SINGLETON_CLASS_ERROR_MESSAGE)
        else:
            Database.__databaseObject = self

    def createDatabaseStructure(self):
        self.createDatabaseTables()
        self.insertDataInRolestable()

    def createDatabaseTables(self):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToCreateRolesTable)
        databaseCursor.execute(queryToCreateTopicsable)
        databaseCursor.execute(queryToCreateClientTable)
        databaseCursor.execute(queryToCreateClientTopicMappingTable)
        databaseCursor.execute(queryToCreateMessageTable)
        self.databaseConnection.commit()
        databaseCursor.close()
        return True
    
    def insertDataInRolestable(self):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToSaveDataInRolesTable)
        self.databaseConnection.commit()
        databaseCursor.close()
        return True

    def saveClientIdInDatabase(self, clientID, clientName):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToSaveClientID, (clientID, clientID, clientName))
        self.databaseConnection.commit()
        databaseCursor.close()
        return True

    def getTopicListFromDatabase(self):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToGetTopicList)
        allRecords = databaseCursor.fetchall()
        allRecords = dict(allRecords)
        allRecords = str(allRecords)
        databaseCursor.close()
        return allRecords

    def getRoleListFromDatabase(self):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToGetRoleList)
        allRecords = databaseCursor.fetchall()
        allRecords = dict(allRecords)
        allRecords = str(allRecords)
        databaseCursor.close()
        return allRecords

    def updateClientTable(self, clientID, RoleID, TopicID):
        databaseCursor = self.databaseConnection.cursor()
        currentTime = datetime.now()
        databaseCursor.execute(
            queryToSaveDataInClientTable, (clientID, RoleID,TopicID))
        self.databaseConnection.commit()
        databaseCursor.close()

    def getLastMessageWatchTime(self, clientAttributeObject):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToGetLastWatchTime,(clientAttributeObject.clientId,clientAttributeObject.topicId, clientAttributeObject.SubscriberRoleId))
        watchTime = databaseCursor.fetchall()
        if (watchTime[0][0] == None):
            watchTime = 0
            return (watchTime)
        databaseCursor.close()
        return (watchTime[0][0])

    def getTopicIdFromDatabase(self, topicName):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToGetTopicID, topicName)
        topicId = databaseCursor.fetchall()
        databaseCursor.close()
        return (topicId[0][0])

    def getRoleId(self, roleName):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToGetRoleID, roleName)
        roleId = databaseCursor.fetchall()
        databaseCursor.close()
        return (roleId[0][0])

    def checkExistanceOfSubscriber(self, clientID, SUBSCRIBER_ROLE_ID, topicId):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToCheckExistanceOfSubscriber, (clientID, SUBSCRIBER_ROLE_ID, topicId))
        isSubscriberExists = databaseCursor.fetchall()
        databaseCursor.close()
        return (isSubscriberExists[0][0])   

    def updateMessageWatchTime(self, clientAttributeObject, SeenMessageTime):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToUpdateMessageWatchTime, (SeenMessageTime, clientAttributeObject.clientId , clientAttributeObject.topicId, clientAttributeObject.SubscriberRoleId))
        self.databaseConnection.commit()
        databaseCursor.close()

    def savePublisherDataToDatabase(self, clientJsonMessage, clientID, topicId):
        databaseCursor = self.databaseConnection.cursor()
        timeStamp = datetime.now()
        databaseCursor.execute(queryToPublishedMessageToDatabase,(clientJsonMessage[MESSAGE_DATA], int(topicId), int(clientID), timeStamp))
        self.databaseConnection.commit()
        databaseCursor.close()

    def saveServerClientDataToDataBase(self, clientID, RoleID, TopicID):
        self.updateClientTable(clientID, RoleID, TopicID)

    def getNoOfTopicsFromDatabase(self):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToGetToipcCount)
        topicCount = databaseCursor.fetchall()
        databaseCursor.close()
        return (topicCount[0][0])

    def checkExistanceOfTopic(self, topicName):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToCheckExistanceOfTopic, topicName)
        isTopicExists = databaseCursor.fetchall()
        databaseCursor.close()
        return (isTopicExists[0][0]) 

    def addTopicInDatabase(self, topicName):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToAddTopicToDatabase, (topicName))
        self.databaseConnection.commit()
        databaseCursor.close()
        return True

    def updateStatusOfDeadMessage(self, TIME_LIMIT_FOR_DEAD_MESSAGE):
        databaseCursor = self.databaseConnection.cursor()
        databaseCursor.execute(queryToUpdateDeadMessageStatus, (TIME_LIMIT_FOR_DEAD_MESSAGE))
        self.databaseConnection.commit()
        databaseCursor.close()  

    def __del__(self):
        if self.databaseConnection != None:
            self.databaseConnection.close()
