import sys
import os
sys.path.append(os.getcwd())
from DataBase.DatabaseConstants import *



queryToCreateTopicsable = '''IF OBJECT_ID(N'dbo.Topics', N'U') IS NOT NULL
BEGIN
	PRINT 'Table Exists'
END
ELSE
BEGIN 
CREATE TABLE [dbo].[Topics](
	[TopicID] [int] IDENTITY(1,1) NOT NULL,
	[TopicName] [varchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[TopicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
'''

queryToCreateRolesTable = '''IF OBJECT_ID(N'dbo.Roles', N'U') IS NOT NULL
BEGIN
	PRINT 'Table Exists'
END
ELSE
BEGIN 
	CREATE TABLE [dbo].[Roles](
	[RoleID] [int] NOT NULL,
	[RoleName] [varchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END'''

queryToCreateClientTable = '''IF OBJECT_ID(N'dbo.Client', N'U') IS NOT NULL
BEGIN
	PRINT 'Table Exists'
END
ELSE
BEGIN 
CREATE TABLE [dbo].[Client](
	[ClientID] [int] NOT NULL,
	[ClientName] [nvarchar](125) NULL,
PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
'''

queryToCreateClientTopicMappingTable = '''IF OBJECT_ID(N'dbo.ClientRoleMapping', N'U') IS NOT NULL
BEGIN
	PRINT 'Table Exists'
END
ELSE
BEGIN 
CREATE TABLE [dbo].[ClientRoleMapping](
	[ClientID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[TopicID] [int] NOT NULL,
	[MessageWatchTime] [datetime] NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[ClientRoleMapping]  WITH CHECK ADD  CONSTRAINT [FK__Client__RoleID__7F2BE32F] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([RoleID])

ALTER TABLE [dbo].[ClientRoleMapping] CHECK CONSTRAINT [FK__Client__RoleID__7F2BE32F]

ALTER TABLE [dbo].[ClientRoleMapping]  WITH CHECK ADD FOREIGN KEY([ClientID])
REFERENCES [dbo].[Client] ([ClientID])
END
'''
queryToCreateMessageTable = '''IF OBJECT_ID(N'dbo.Message', N'U') IS NOT NULL
BEGIN
	PRINT 'Table Exists'
END
ELSE
BEGIN 
CREATE TABLE [dbo].[Message](
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](512) NULL,
	[TopicID] [int] NULL,
	[ClientID] [int] NULL,
	[TimeStamp] [datetime] NULL,
	[IsDead] [bit] NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Message]  WITH CHECK ADD FOREIGN KEY([ClientID])
REFERENCES [dbo].[Client] ([ClientID])

ALTER TABLE [dbo].[Message]  WITH CHECK ADD FOREIGN KEY([TopicID])
REFERENCES [dbo].[Topics] ([TopicID])
END
'''
queryToGetTopicList = """SELECT TopicID, TopicName FROM Topics"""
queryToGetRoleList = """SELECT RoleID, RoleName FROM Roles"""
queryToGetSubscriptionDetails = """SELECT SeenMessageCount FROM ClientRoleMapping Where ClientId = (?) AND TopicID = (?) AND RoleID = (?)"""
queryToGetLastWatchTime = """SELECT MessageWatchTime FROM ClientRoleMapping Where ClientId = (?) AND TopicID = (?) AND RoleID = (?)"""
queryToGetTopicID = """SELECT TopicId FROM Topics where TopicName = (?)"""
queryToGetRoleID = """SELECT RoleId FROM Roles where RoleName = (?)"""
queryToCheckExistanceOfSubscriber = """select count(*) from ClientRoleMapping where ClientID = (?) AND RoleID = (?) AND TopicID = (?)"""
queryToCheckExistanceOfTopic = """select count(*) from Topics where TopicName = (?)"""
queryToGetToipcCount = """select count(*) from Topics"""

queryToUpdateMessageWatchTime = """UPDATE ClientRoleMapping SET MessageWatchTime = (?) where ClientID = (?) AND TopicID = (?) AND RoleID = (?)"""
queryToUpdateDeadMessageStatus = """UPDATE Message SET IsDead = (1) where DATEDIFF(SECOND, TimeStamp , Getdate()) > (?)"""

queryToSaveDataInClientTable = """INSERT INTO ClientRoleMapping (ClientId,RoleID,TopicID) VALUES (?,?,?)"""
queryToSaveDataInTopicsTable = """INSERT INTO Topics (TopicID, TopicName) VALUES ('Technical'), ('NonTechnical'), ('Accounts')"""
queryToSaveDataInRolesTable = """IF NOT EXISTS (SELECT * FROM Roles )
BEGIN
INSERT INTO Roles (RoleID, RoleName) VALUES (1,'Publisher'),(2,'Subscriber')
END"""
queryToPublishedMessageToDatabase = """INSERT INTO Message (Message, TopicID, ClientID, TimeStamp) VALUES (?,?,?,?)"""
queryToAddTopicToDatabase = "INSERT INTO Topics (TopicName) VALUES (?)"
queryToSaveClientID = """
IF NOT EXISTS (SELECT * FROM Client where ClientID = (?))
BEGIN
    INSERT INTO Client (ClientID, ClientName)
    VALUES (?,?)
END
"""
queryToDropExistTestDatabase = "USE [master] ALTER DATABASE [IMQ_TEST] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE USE [master] DROP DATABASE IF EXISTS ["+TEST_DATABASE_NAME+"]"
queryToCreatetestDataBase = "CREATE DATABASE [" + TEST_DATABASE_NAME + "]"
queryToCreateTestTable = "USE [" + TEST_DATABASE_NAME + "] CREATE TABLE Test (ID int, Name varchar(50))"
queryToInsertTestData = "USE [" + TEST_DATABASE_NAME + "] INSERT INTO Test VALUES(1, 'Devraaj'),(2, 'Gurjar')"
queryToDropTestDatabase = "USE [master] ALTER DATABASE [IMQ_TEST] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE USE [master] DROP DATABASE [" + TEST_DATABASE_NAME + "]"



 
