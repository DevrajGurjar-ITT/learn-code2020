import sys
import os
sys.path.append(os.getcwd())
import unittest
import socket
from contextlib import contextmanager
from unittest.mock import patch
from Constants.Constants import *
from Server.ServerUtility import *


class ServerUtilityUnitTest(unittest.TestCase):

    __serverUtilityObject = ServerUtility()
    __queueObject = Queue()
    imQueue = queue.Queue(MAX_QUEUE_SIZE)

    def testIsInstanceOfServerutility(self):
        self.assertIsInstance(self.__serverUtilityObject, ServerUtility)

    def testIsInstanceOfQueue(self):
        self.assertIsInstance(self.__queueObject, Queue)

    def testGetResponseObject(self):
        response = self.__serverUtilityObject.getResponseObject(MESSAGE_DATA)
        self.assertEqual(response.data , MESSAGE_DATA)

    def testClientRole(self):
        response = self.__serverUtilityObject.decideClientRole(MOCK_JSON_DATA)
        self.assertTrue(response)


if __name__ == '__main__':
    unittest.main()
