import sys
import os
sys.path.append(os.getcwd())
import unittest
import socket
from contextlib import contextmanager
from unittest.mock import patch
from Constants.Constants import *
from Server.ServerUtility import *
from Constants.UnitTestConstants import *


class ServerUtilityUnitTest(unittest.TestCase):

    __serverUtilityObject = ServerUtility()
    __queueObject = Queue()
    imQueue = queue.Queue(MAX_QUEUE_SIZE)

    def testIsInstanceOfQueue(self):
        self.assertIsInstance(self.__queueObject, Queue)

    def testIsInstanceOfServerUtility(self):
        self.assertIsInstance(self.__serverUtilityObject, ServerUtility)

    def testInitializeQueue(self):
        queueReceived = self.__queueObject.initializeQueue()
        self.assertEqual(type(queueReceived), type(self.imQueue))

    def testIsQueueEmpty(self):
        response = self.__queueObject.isQueueEmpty(self.imQueue)
        self.assertTrue(response)

    def testIsQueueFull(self):
        response = self.__queueObject.isQueueFull(self.imQueue)
        self.assertFalse(response)

    def testaddMessageToQueue(self):
        response = self.__queueObject.addMessageToQueue(self.imQueue ,MOCK_NAME)
        self.assertTrue(response)

    def testEmptyQueueAfterInsertion(self):
        response = self.__queueObject.isQueueEmpty(self.imQueue)
        self.assertTrue(response)

    def testgetMessageFromQueue(self):
        response = self.__queueObject.getMessageFromQueue(self.imQueue)
        self.assertEqual(response, MOCK_NAME)

    def testaddMessageToDeadLetterQueue(self):
        response = self.__queueObject.addMessageToDeadQueue(self.imQueue ,MOCK_NAME)
        self.assertTrue(response)

    def testgetMessageFromDeadQueue(self):
        response = self.__queueObject.getMessageFromQueue(self.imQueue)
        self.assertEqual(response, MOCK_NAME)

if __name__ == '__main__':
    unittest.main()