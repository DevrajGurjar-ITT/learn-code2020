import sys
import os
sys.path.append(os.getcwd())
import unittest
import socket
from contextlib import contextmanager
from unittest.mock import patch
from Constants.Constants import *
from Server.ServerService import *
import ParserPKG as Parser
from Constants.UnitTestConstants import *

class ServerServiceUnitTest(unittest.TestCase):

    serverServiceObject = ServerService(None, None)
    __parserObject = Parser.Parser()

    def testIsInstanceOfServerService(self):
        self.assertIsInstance(self.serverServiceObject, ServerService)

    def testIsInstanceOfParser(self):
        self.assertIsInstance(self.__parserObject, Parser.Parser)

    def testSerializeToJsonConvertor(self):
        response = self.__parserObject.serializeToJsonConvertor(MOCK_MESSAGE)
        self.assertEqual(response, RESPONSE_MOCK_MESSAGE)

    def testDeserializeJsonFormattedData(self):
        serializeResponse = self.__parserObject.serializeToJsonConvertor(MOCK_MESSAGE)
        deserializeResponse = self.__parserObject.deserializeJsonFormattedData(serializeResponse)
        self.assertEqual(deserializeResponse, MOCK_MESSAGE)


if __name__ == '__main__':
    unittest.main()
