import sys
import os
sys.path.append(os.getcwd())
import unittest
import socket
from contextlib import contextmanager
from unittest.mock import patch
from Constants.ExceptionStringLiterals import *
from Constants.Constants import *
from UnitTests.MockDatabase import MockDB
from DataBase.Database import *
from Constants.UnitTestConstants import *


class Server_test(MockDB, unittest.TestCase):

    mockDatabaseObject = unittest.mock.create_autospec(Database)

    mockDatabaseObject.insertDataInRolestable.return_value = True

    def testCreateDatabaseTables(self):
        with self.mock_db_config:
            databaseObject = Database.getInstance()
            self.assertTrue(databaseObject.createDatabaseTables())
            self.assertTrue(databaseObject.insertDataInRolestable())
            self.assertTrue(databaseObject.getRoleId(MOCK_ROLE))
            self.assertTrue(databaseObject.saveClientIdInDatabase(1,MOCK_NAME))
            self.assertTrue(databaseObject.getTopicListFromDatabase)
            self.assertTrue(databaseObject.addTopicInDatabase(MOCK_TOPIC))
            self.assertFalse(databaseObject.updateClientTable(1,1,1))
            self.assertEqual(databaseObject.checkExistanceOfTopic(MOCK_TOPIC), 1)
            self.assertTrue(databaseObject.saveClientIdInDatabase(1,MOCK_NAME))
            self.assertEqual(databaseObject.getTopicIdFromDatabase(MOCK_TOPIC),1)

    def testInsertDataInRolestable(self):
        self.assertTrue(self.mockDatabaseObject.insertDataInRolestable())


if __name__ == '__main__':
    unittest.main()
