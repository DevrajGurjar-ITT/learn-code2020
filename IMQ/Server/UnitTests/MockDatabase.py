import sys
import os
sys.path.append(os.getcwd())
import pyodbc
from unittest import TestCase
from mock import patch
from DataBase.DatabaseConstants import *
from DataBase.DatabaseQueries import *

class MockDB(TestCase):
    
    @classmethod
    def setUpClass(cls):
        databaseConnection = pyodbc.connect(TEST_DATABASE_CONNECTION_STRING)
        cursor = databaseConnection.cursor()
        databaseConnection.autocommit = True
        cursor.execute(queryToDropExistTestDatabase)
        databaseConnection.autocommit = True
        cursor.close()
        cursor = databaseConnection.cursor()
        cursor.execute(queryToCreatetestDataBase)
        cursor.execute(queryToCreateTestTable)
        cursor.execute(queryToInsertTestData)
        databaseConnection.commit()
        cursor.close()
        databaseConnection.close()

    mock_db_config = patch.dict(DATABASE_CONNECTION_CONFIG, TEST_CONFIG)

    @classmethod
    def tearDownClass(cls):
        databaseConnection = pyodbc.connect(TEST_DATABASE_CONNECTION_STRING)
        cursor = databaseConnection.cursor()
        databaseConnection.autocommit = True
        cursor.execute(queryToDropTestDatabase)
        databaseConnection.commit()
        cursor.close()
        databaseConnection.close()


