import sys
import os
sys.path.append(os.getcwd())
import unittest
import socket
from contextlib import contextmanager
from unittest.mock import patch
from Server.ServerSocket import *
from unittest.mock import Mock, create_autospec, patch
from Constants.UnitTestConstants import *


class ServerSocketUnitTest(unittest.TestCase):

    __serverSocketObject = ServerSocket()
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def testIsInstanceOfServerr(self):
        self.assertIsInstance(self.__serverSocketObject, ServerSocket)

    def testSocketCreation(self):
        socketReceived = self.__serverSocketObject.createSocket()
        self.assertEqual(type(socketReceived), type(self.serverSocket))
        socketReceived.close()

    @patch('builtins.print')
    def testSocketStatus(self, mock_print):    
        self.__serverSocketObject.checkSocketStatus(self.serverSocket)
        mock_print.assert_called_with(SOCKET_SUCCESS_MESSAGE)

    def testSocketBinding(self):
        self.assertIsNone(self.__serverSocketObject.bindServerSocket(self.serverSocket))

    def testHostIP(self):
        self.assertTrue(socket.gethostbyname(MOCK_LOCALHOST))

    def testcloseSocket(self):
        self.assertTrue(self.__serverSocketObject.closeSocket(self.serverSocket))

    def testreceiveMessageFromClient(self):
        message = str.encode(MOCK_NAME)
        mockSocket = create_autospec(socket.socket)
        mockSocket.recv.return_value = message
        self.__serverSocketObject.receiveMessageFromClient(mockSocket)
        assert mockSocket.recv.called
    

if __name__ == '__main__':
    unittest.main()
