import sys
import os
path = os.path.abspath(__file__)
path = path + "/../.."
sys.path.append(path)
from UnitTests.ServerServiceUnitTest import *
from UnitTests.ServerSocketUnitTest import *
from UnitTests.ServerUtilityUnitTest import *
from UnitTests.QueueUnittest import *


if __name__ == '__main__':
    unittest.main()