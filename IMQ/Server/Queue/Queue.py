import queue
from Constants.Constants import *
from DataBase.Database import *

class Queue:

    def initializeQueue(self):
        imQueue = queue.Queue(MAX_QUEUE_SIZE)
        return imQueue

    def getQueueSize(self, imQueue):
        return imQueue.qsize()

    def addMessageToQueue(self, imQueue, message):
        imQueue.put(message)
        return True

    def addMessageToDeadQueue(self, imQueue, message):
        imQueue.put(message)
        return True

    def isQueueFull(self, imQueue):
        return imQueue.full() 

    def getMessageFromQueue(self, imQueue):
        message = imQueue.get()
        return message

    def isQueueEmpty(self, imQueue):
        return(imQueue.empty())

    def queueManager(self, databaseObj):
        queueObject = Queue()
        listOfQueue = [queueObject]
        topicCount = databaseObj.getNoOfTopicsFromDatabase()
        for index in range(0, topicCount):
            queue = queueObject.initializeQueue()
            listOfQueue.append(queue)
        return listOfQueue
