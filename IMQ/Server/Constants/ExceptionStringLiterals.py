SOCKET_CREATE_ERROR_MESSAGE = 'Error while creating the Socket!!'
CONNECTION_BIND_ERROR_MESSAGE = 'Error while socket binding to port'
SERVER_LISTEN_ERROR_MESSAGE = 'Server in not able to listen to port'
SERVER_CONNECTION_ERROR_MESSAGE = 'Server is not able to accept connection from client'
SOCKET_CLOSE_ERROR_MESSAGE = 'Error while closing the socket'
CONNECT_TO_CLIENT_ERROR_MESSAGE = "Oops! Something went wrong, please try again later..."
DATABASE_CONNECTION_EXCEPTION = "Not able to save the data in table, please check the Database Connection"
RECEIVE_MESSAGE_ERROR = 'A New Exception has occured: Error while receiving message from Server'
SEND_MESSAGE_ERROR = 'A New Exception has occured: Error while sending message to Server'
PUBLICATION_ERROR_MESSAGE = '!!SERVER ERROR!! An Error occured while publishing Messages on Server'
SUBSCRIPTION_ERROR_MESSAGE = '!!SERVER ERROR!! An Error occured while Subscribing Messages on Server'
DATABASE_ERROR_MESSAGE = "An Error Occured while performing operation on Database"
SERVER_CLOSE_ERROR_MESSAGE = "Error Occured So Closing The Connection"
THREAD_CLOSE_ERROR_MESSAGE = "Error Occured Thread Got Closed"

