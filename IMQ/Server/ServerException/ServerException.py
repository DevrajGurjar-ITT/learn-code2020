import sys
import os
sys.path.append(os.getcwd())
from Constants.ExceptionStringLiterals import *


class ServerException(Exception):

    def printExceptionMessage(self):
        pass

class SocketErrorException(ServerException):
    
    def printExceptionMessage(self):
        print(SOCKET_CREATE_ERROR_MESSAGE)

class ConnectionFailedException(ServerException):

    def printExceptionMessage(self):
        print(CONNECT_TO_CLIENT_ERROR_MESSAGE)

class ConnectionBindErrorException(ServerException):

    def printExceptionMessage(self):
        print(CONNECTION_BIND_ERROR_MESSAGE)

class SocketListenErrorException(ServerException):
    
    def printExceptionMessage(self):
        print(SERVER_LISTEN_ERROR_MESSAGE)

class SendMessageErrorException(ServerException):
    
    def printExceptionMessage(self):
        print(SEND_MESSAGE_ERROR)

class RceiverMessageErrorException(ServerException):
    
    def printExceptionMessage(self):
        print(RECEIVE_MESSAGE_ERROR)

class PublicationErrorException(ServerException):
    
    def printExceptionMessage(self):
        print(PUBLICATION_ERROR_MESSAGE)

class SubscriptionErrorException(ServerException):
    
    def printExceptionMessage(self):
        print(SUBSCRIPTION_ERROR_MESSAGE)

class DatabaseErrorException(ServerException):
    
    def printExceptionMessage(self):
        print(DATABASE_ERROR_MESSAGE)

class CloseThreadException(ServerException):
    
    def printExceptionMessage(self):
        print(THREAD_CLOSE_ERROR_MESSAGE)

class CloseSocketException(ServerException):
    
    def printExceptionMessage(self):
        print(SOCKET_CLOSE_ERROR_MESSAGE)
