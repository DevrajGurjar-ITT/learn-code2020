from dataclasses import dataclass
from IMQProtocol.IMQProtocol import *


@dataclass
class Request(IMQProtocol):
    requestType: str = REQUEST_TYPE
    
