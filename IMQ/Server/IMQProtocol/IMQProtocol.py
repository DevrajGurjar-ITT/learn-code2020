import sys
import os
sys.path.append(os.getcwd())
from Constants.Constants import *
from dataclasses import dataclass


@dataclass
class IMQProtocol:
    data: str
    sourceURL: str
    destinationURL: str
    dataFormat: str = DATA_FORMAT
    version: str = VERSION
