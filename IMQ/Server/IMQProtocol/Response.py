from dataclasses import dataclass
from IMQProtocol.IMQProtocol import *


@dataclass
class Response(IMQProtocol):
    status: str = STATUS
