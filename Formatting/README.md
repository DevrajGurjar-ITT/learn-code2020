# README #

## General Formatting Rules ##

### Vertical Formatting ###
* **Vertical openess:** Each line represents an expression, and each group of lines represents a complete thought. Those thoughts should be separated from each other with blank lines.
* **Vertical density:** Lines of code that are tightly related should appear vertically dense. 
* **Vertical distance:** Concepts that are closely related should be kept vertically close to each other.
* **Variable declarations:** Variables should be declare at the top of the function unless its scope is local to a particular block.
* **Instance variable:** Instance Variables should be declare at the top of the class or function.
* **Dependent function:** Caller should be above the callie and their distance should be near if possible.
* **Conceptual affinity:** Method overloading is an expample of conceptual affinity. The stronger that affinity, the less vertical distance there should be between them.

### Horizontal Formatting ###
* **Horizontal openess and density:** We use horizontal white space to associate things that are strongly related and disassociate things that are more weakly related.
* **Methods & Functions:** There should be no space between the function names and the opening parenthesis. Separate arguments within the function call and return parenthesis by the comma and show that the arguments are separate.
* **Assignment operators:** Add spaces to the left and right of the operator.
* **Logical operators:** Add spaces to the left and right of the logical operator.
* **Mathematical operators:** Add spaces to the left and right of the mathematical operator.
* **Concatination operator:** Add spaces to the left and right.
* **Indentation:** By consistently indenting code that is subordinate to or controlled by surrounding code, you send a clear visual message to the human reader about the structure of your code.

## Python specific Formatting Rules ##
* **Naming convension:** 
* **Function name:** Snake casing. Use a lowercase word or words. Separate words by underscores to improve readability.
* **Variable name:** Snake casing
* **Class name:** Camel casing. Start each word with a capital letter. Do not separate words with underscores.
* **Constant:** Snake Upper casing. Use an uppercase single letter, word, or words. Separate words with underscores to improve readability.
* **Module:** Snake casing
* **Package:** Use a short, lowercase word or words. Do not separate words with underscores.
* **Code Layout:**
* **Blank Lines:** Surround top-level functions and classes with two blank lines Surround method definitions inside classes with a single blank line. Use blank lines sparingly inside functions to show clear steps. 	Maximun line length: Suggested to use 79 charecters max
* **Indentation:** Use 4 consecutive spaces to indicate indentation. Prefer spaces over tabs.

## Tool Used For Formatting in Python ##
* **PEP8 online**